import React from 'react'
import Link from '../Link'
import styles from './MyLinks.module.scss'
import { useAppSelector } from '../../hooks/useAppSelector'

const MyLinks: React.FC = () => {
  const myList = useAppSelector((state) => state.links.myList)

  if(myList.length == 0) {
    return null
  }

  return (
    <div className={styles.box}>
      <h5 className={styles.title}>Мои ссылки</h5>
      <ul className={styles.list}>
        {myList.map((item, index) => (
          <li key={item.id} className={styles.item}>
            <Link index={item.id} url={item.url} short_url={item.short_url} clicks={item.clicks} />
          </li>
        ))}
      </ul>
    </div>
  )
}

export default MyLinks