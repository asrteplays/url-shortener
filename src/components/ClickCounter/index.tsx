import React from 'react'
import styles from './ClickCounter.module.scss'

type ClickCounterProps = {
  clicks: number
}

const ClickCounter: React.FC<ClickCounterProps> = ({ clicks }) => {
  return (
    <div className={styles.box}>
      <span className={styles.count}>{clicks}</span>
    </div>
  )
}

export default ClickCounter