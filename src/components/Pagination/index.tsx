import React from 'react'
import PaginationItem from '../PaginationItem'
import styles from './Pagination.module.scss'
import {createCountPages} from '../../utils/pagination'
import {useAppDispatch} from '../../hooks/useAppDispatch'
import {fetchLinks} from '../../store/links/asyncActions'

type PaginationProps = {
  total: number
}

const ItemDots: React.FC = () => {
  return (
    <div className={styles.item + ' ' + styles.item__dots}>...</div>
  )
}

const Pagination: React.FC<PaginationProps> = ({ total }) => {
  const [paginBorders, setPaginBorders] = React.useState({start: 1, end: 4})

  const dispatch = useAppDispatch();

  const pages = React.useMemo(() => {
    return createCountPages(total)
  }, [total])

  const [activePage, setActivePage] = React.useState(1);

  const handleChangePage = (item: number) => {
    if(item == paginBorders.end && paginBorders.end < pages.length - 1) {
      setPaginBorders({start: paginBorders.start + 1, end: paginBorders.end + 1})
    }

    if(item - 1 == paginBorders.start && paginBorders.start > 1) {
      setPaginBorders({start: paginBorders.start - 1, end: paginBorders.end - 1})
    }

    if(item == 1) {
      setPaginBorders({start: 1, end: 4})
    }

    if(item == pages.length) {
      setPaginBorders({start: pages.length - 4, end: pages.length - 1})
    }

    if(item != activePage) {
      dispatch(fetchLinks({first: 10, page: item}))
      setActivePage(item)
    }
  }

  const slicePagin = React.useMemo(() => {
    return pages.slice(paginBorders.start, paginBorders.end)
  }, [pages, paginBorders.start, paginBorders.end])

  return (
    <div className={styles.box}>
      {pages.slice(0, 1).map((item, index) => (
        <div onClick={() => handleChangePage(item)} key={total+'#'+item} className={styles.item}>
          <PaginationItem active={activePage == item ? true : false} number={item} />
        </div>
      ))}
      {paginBorders.start > 1 && <ItemDots />}
      {slicePagin.map((item, index) => {
        return (
          <div onClick={() => handleChangePage(item)} key={total+'#'+item} className={styles.item}>
            <PaginationItem active={activePage == item ? true : false} number={item} />
          </div>
        )
      })}
      {activePage < pages.length - 1 && <ItemDots />}
      {pages.slice(-1).map((item, index) => (
        <div onClick={() => handleChangePage(item)} key={total+'#'+item} className={styles.item}>
          <PaginationItem active={activePage == item ? true : false} number={item} />
        </div>
      ))}
    </div>
  )
}

export default Pagination