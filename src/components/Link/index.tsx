import React from 'react'
import ClickCounter from '../ClickCounter'
import styles from './Link.module.scss'

type LinkProps = {
  index: number,
  url: string,
  short_url: string,
  clicks: number
}

const Link: React.FC<LinkProps> = (props) => {
  const {index, url, short_url, clicks} = props;

  return (
    <div className={styles.box}>
      <span className={styles.number}>{index}</span>
      <a className={styles.link} href={url} rel="noreferrer" target="_blank">{url}</a>
      <a className={styles.link} href={short_url} rel="noreferrer" target="_blank">{short_url}</a>
      <div className={styles.counter}>
        <ClickCounter clicks={clicks} />
      </div>
    </div>
  )
}

export default Link