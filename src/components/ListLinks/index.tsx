import React from 'react'
import styles from './ListLinks.module.scss'
import { useAppSelector } from '../../hooks/useAppSelector'
import Link from '../Link'
import Pagination from '../Pagination'

const ListLinks: React.FC = () => {
  const list = useAppSelector((state) => state.links.list)
  const paginatorInfo = useAppSelector((state) => state.links.paginatorInfo)

  return (
    <div className={styles.box}>
      <h5 className={styles.title}>Список ссылок</h5>
      <ul className={styles.list}>
        {list.map((item, index) => (
          <li key={item.id} className={styles.item}>
            <Link 
              clicks={item.clicks} 
              index={item.id} 
              short_url={item.short_url} 
              url={item.url} 
            />
          </li>
        ))}
      </ul>
      <Pagination total={paginatorInfo.lastPage} />
    </div>
  )
}

export default ListLinks