import React from 'react'
import styles from './Form.module.scss';
import {useForm} from 'react-hook-form';
import { useAppDispatch } from '../../hooks/useAppDispatch';
import { shortLink } from '../../store/links/asyncActions';
import { useAppSelector } from '../../hooks/useAppSelector';

const Form = () => {
  const dispatch = useAppDispatch();
  const {register, formState: {errors}, handleSubmit, reset} = useForm({mode: 'onSubmit'})

  const onSubmit = (data: any) => {
    dispatch(shortLink(data.link));
    reset()
  }

  const status = useAppSelector((state) => state.links.status);
  
  return (
    <div className={styles.box}>
      <h5 className={styles.title}>Введите ссылку</h5>
      <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        <div className={styles.form__controls}>
          <input 
            className={styles.input} 
            type="text" 
            {...register('link', {
              required: 'Введите ссылку',
              pattern: {
                value: /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g,
                message: 'Введите валидную ссылку',
              },
            })}
          />
          {errors?.link && 
            <span className={styles.error}>{errors?.link.type == 'required' ? 'Введите ссылку' : 'Введите валидную ссылку'}</span>
          }
          {status == 'error' && <span className={styles.error}>К сожалению, что-то пошло не так попробуйте еще раз</span>}
          <button className={styles.btn}>Сократить</button>
        </div>
      </form>
    </div>
  )
}

export default Form