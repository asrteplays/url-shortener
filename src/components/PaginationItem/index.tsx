import React from 'react'
import styles from './PaginationItem.module.scss'

type PaginationItemProps = {
  number: number,
  active: boolean
}

const PaginationItem: React.FC<PaginationItemProps> = ({number, active}) => {
  return (
    <div className={active ? styles.box + ' ' + styles.active : styles.box}>
      <span className={styles.number}>{number}</span>
    </div>
  )
}

export default PaginationItem