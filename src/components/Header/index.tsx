import React from 'react'
import styles from './Header.module.scss'

const Header: React.FC = () => {
  return (
    <div className={styles.box}>
      <h1 className={styles.title}>Сокращатель</h1>
    </div>
  )
}

export default Header