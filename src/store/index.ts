import { configureStore, ThunkAction, Action, combineReducers } from '@reduxjs/toolkit';
import links from './links/slice'

const rootReducer = combineReducers({
  links 
})

export const store = configureStore({
  reducer: rootReducer
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
