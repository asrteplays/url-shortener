import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { LinksSliceState, Status } from "../../types/links";
import { fetchLinks, shortLink } from "./asyncActions";

const initialState: LinksSliceState = {
  list: [],
  myList: [],
  paginatorInfo: {
    count: 0,
    currentPage: 0,
    firstItem: 0,
    hasMorePages: true,
    lastItem: 0,
    lastPage: 0,
    perPage: 0,
    total: 0,
  },
  status: Status.LOADING
}

type ClicksPayload = {
  id: number;
  clicks: number;
};

const links = createSlice({
  name: 'links',
  initialState,
  reducers: {
    updateClicks(state, action: PayloadAction<ClicksPayload>){
      const updateList = state.list.map((item, index) => {
        if(item.id == action.payload.id) {
          return {
            ...item,
            clicks: action.payload.clicks
          }
        }
        return item
      })
      state.list = updateList;
      if(state.myList.length > 0) {
        const updateList = state.myList.map((item, index) => {
          if(item.id == action.payload.id) {
            return {
              ...item,
              clicks: action.payload.clicks
            }
          }
          return item
        })
        state.myList = updateList;
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchLinks.pending, (state, action) => {
      state.status = Status.LOADING
    });
    builder.addCase(fetchLinks.rejected, (state, action) => {
      state.status = Status.ERROR
    });
    builder.addCase(fetchLinks.fulfilled, (state, action) => {
      state.status = Status.SUCCESS
      state.list = action.payload.data;
      state.paginatorInfo = action.payload.paginatorInfo
    });
    builder.addCase(shortLink.pending, (state, action) => {
      state.status = Status.LOADING
    });
    builder.addCase(shortLink.rejected, (state, action) => {
      state.status = Status.ERROR
    });
    builder.addCase(shortLink.fulfilled, (state, action) => {
      state.status = Status.SUCCESS
      state.myList.push(action.payload)
    });
  }
})

export const {updateClicks} = links.actions

export default links.reducer