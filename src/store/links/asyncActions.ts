import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { ShortUrl, FetchLinksParams, FetchLinks } from '../../types/links';

export const shortLink = createAsyncThunk(
  'link/shortLink',
  async(url: string) => {
    const { data } = await axios({
      url: 'http://test-task.profilancegroup-tech.com/graphql',
      method: 'post',
      data: {
        query: `
          mutation ShortLink($url: String) {
            shorten_url(url: $url) {
              short_url {
                  id
                  url
                  short_url
                  clicks
                  created_at
                  updated_at
              }
            }
          }
          `,
        variables: {
          "url": url,
        }
      }
    })

    return data.data.shorten_url.short_url
  }
)

export const fetchLinks = createAsyncThunk<FetchLinks, FetchLinksParams>(
  'links/fetchLinks',
  async (params) => {
    const { first, page } = params;
    
    const {data} = await axios({
      url: 'http://test-task.profilancegroup-tech.com/graphql',
      method: 'post',
      data: {
        query: `
          query GetLinks($first: Int, $page: Int) {
              short_urls (first: $first, page: $page) {
                  data {
                    id
                    url
                    short_url
                    clicks
                    created_at
                    updated_at
                  }
                  paginatorInfo {
                    count
                    currentPage
                    firstItem
                    hasMorePages
                    lastItem
                    lastPage
                    perPage
                    total
                  }
              }
          }
          `,
        variables: {
          "first": first,
          "page": page
        }
      }
    })

    return data.data.short_urls
  },
);