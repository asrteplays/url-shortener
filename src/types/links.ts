export type ShortUrl = {
  id: number,
  url: string,
  short_url: string,
  clicks: number,
  created_at: string,
  updated_at: string,
}

export interface short_url_result {
  short_url: ShortUrl
  operation_status: string
}

export enum Status {
  LOADING = 'loading',
  SUCCESS = 'completed',
  ERROR = 'error',
}

export interface PaginatorInfo {
  count: number,
  currentPage: number,
  firstItem: number
  hasMorePages: boolean,
  lastItem: number,
  lastPage: number,
  perPage: number,
  total: number,
}

export interface LinksSliceState {
  list: ShortUrl[],
  myList: ShortUrl[],
  paginatorInfo: PaginatorInfo
  status: Status;
}

export interface FetchLinks {
  data: ShortUrl[],
  paginatorInfo: PaginatorInfo
}

export type FetchLinksParams = {
  first: number,
  page: number
}

