export const createCountPages = (total: number) => {
  let pages = [];
  for (let i = 0; i < total; i++) {
    pages.push(i + 1)
  }

  return pages
}