import React from 'react';
import Header from './components/Header';
import styles from './assets/styles/App.module.scss'
import MyLinks from './components/MyLinks';
import ListLinks from './components/ListLinks';
import { useAppDispatch } from './hooks/useAppDispatch';
import { fetchLinks } from './store/links/asyncActions';
import Form from './components/Form';
import { updateClicks } from './store/links/slice';

import Echo from 'laravel-echo';
window.io = require('socket.io-client');

declare global {
  interface Window {
    Echo: any;
    io: any
  }
}

function App() {
  const dispatch = useAppDispatch()

  React.useEffect(() => {
    dispatch(fetchLinks({first: 10, page: 1}))
  }, [])

  if (typeof window.io !== 'undefined') {
    window.Echo = new Echo({
      broadcaster: 'socket.io',
      host: 'http://test-task.profilancegroup-tech.com:6002',
    });
  }

  window.Echo.channel('btti_database_short_urls')
    .listen('.new_click', (data: any) => {
      dispatch(updateClicks({id: data.short_url.id, clicks: data.short_url.clicks}))
    })

  return (
    <div className={styles.app}>
      <div className={styles.container}>
        <header className={styles.header}>
          <Header />
        </header>
        <main className={styles.main}>
          <div className={styles.content}>
            <div className={styles.content__item}>
              <div className={styles.form}>
                <Form />
              </div>
              <MyLinks />
            </div>
            <div className={styles.content__item}>
              <ListLinks />
            </div>
          </div>
        </main>
      </div>
    </div>
  );
}

export default App;
